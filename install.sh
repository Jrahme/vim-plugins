#this assumes you're using a distro with apt-get because I'm lazy

sudo apt-get install -y $(cat package-dependencies)
bash $(cat go-dependencies)
git submodule init
git submodule update
ln -s $HOME/.vim/pack $PWD/vim-home/pack
ln -s $HOME/.vim/colors $PWD/vim-home/colors
#install the clojure-lsp
wget https://github.com/snoe/clojure-lsp/releases/download/release-20190614T052638/clojure-lsp > ~/.bin/clojure-lsp
chomd +x ~/.bin/clojure-lsp
